from fastapi import FastAPI
from fastapi.responses import JSONResponse
from Secweb import SecWeb

app = FastAPI()
SecWeb(app=app)

@app.get("/")
async def root():
    return {"message": 'Hello DAST'}


@app.get("/dast")
async def dast():
    content = {"message": "Hello DAST Header"}
    headers = {"Gitlab-On-Demand-DAST": "ab157d29-7b08-4b5a-b38c-ab0de3818afd", "Content-Language": "en-US"}
    return JSONResponse(content=content, headers=headers)
  
  
    
 
