FROM tiangolo/uvicorn-gunicorn-fastapi:python3.10
COPY . /app/
WORKDIR /app
EXPOSE 8000
RUN pip install -r requirements.txt 
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]
